const dynamodbHelper = require("../helper/DynamodbHelper");

module.exports = async (event, context) => {
  const queryParams = event.queryStringParameters;

  let result = null;
  let res = null;
  if (!queryParams) {
    throw new Error("There's no query parameter");
  } else {
    switch (queryParams["action"]) {
      case "getItemById":
        result = await getItemById(event);
        break;
      case "queryItemByCategoryId":
        result = await queryItemByCategoryId(event);
        break;
      case "queryItemByBrandId":
        result = await queryItemByBrandId(event);
        break;
      case "queryItemBySaleOffId":
        result = await queryItemBySaleOffId(event);
        break;
      case "queryItemByRangeAmount":
        result = await queryItemByRangeAmount(event);
        break;
      case "listAllItem":
        result = await listAllItem();
        break;
      default:
        result = [];
        break;
    }
    res = {
      message: "Successful",
      data: result,
    };
  }
  return res;
};

async function getItemById(event) {
  const PK = event.queryStringParameters["PK"] || "";
  const SK = event.queryStringParameters["SK"] || "";

  const params = {
    TableName: process.env.GOODS_TABLE_NAME,
    Key: {
      PK: PK,
      SK: SK,
    },
  };

  return await dynamodbHelper.getItem(params);
}

async function queryItemByBrandId(event) {
  return [];
}

async function queryItemBySaleOffId(event) {
  return [];
}

async function queryItemByRangeAmount(event) {
  return [];
}

async function queryItemByCategoryId(event) {
  const PK = event.queryStringParameters.PK || "";
  const limit = event.queryStringParameters["limit"] || 50;
  const params = {
    TableName: process.env.GOODS_TABLE_NAME,
    KeyConditionExpression: "PK = :pk",
    AttributeExpression: {
      ":pk": PK,
    },
    Limit: limit,
  };

  return await dynamodbHelper.scanAllItems(params);
}

async function listAllItem() {
  const params = {
    TableName: process.env.GOODS_TABLE_NAME,
  };

  return await dynamodbHelper.scanAllItems(params);
}
