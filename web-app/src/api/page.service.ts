import AuthService from '@/api/auth.service';

export default class PageService {
  static async getPages(): Promise<any> {
    const url = process.env.VUE_APP_API_BASE_URL + '/dev/pages';
    // return await AuthService.api.get(url).then((response) => response.data);
    return url;
  }
}
