import os
import sys
import json
import boto3
import argparse
import time

from decimal import Decimal
from json import JSONEncoder 
from get_code_from_product import GetCodeFromProduct


file_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.abspath(file_dir + '/../..')
sys.path.append(os.path.normpath(root_dir))
from tools.aws_service.helper import S3Helper

parser = argparse.ArgumentParser()
parser.add_argument('--force', action='store_true')
parser.add_argument('env_name', type=S3Helper.env_regex_type)
parser.add_argument('--project-name', default='pcw')
parser.add_argument('--profile', default='pcw-admin')
parser.add_argument('--use-container', action='store_true')

args = vars(parser.parse_args())  
start_time = time.time() 

s3_helper = S3Helper(root_dir=root_dir + '/tools/aws_service', args=args)
dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1',)

pages_table = dynamodb.Table('pcw-dev-PAGES')
goods_table = dynamodb.Table('pcw-dev-GOODS')



def put_goods_item_to_db(item): 
    """
        save item after handling to dynamodb
    """ 
    # try: 
    if item: goods_table.put_item(Item= item) 
    # except: 
    #     print('delete')
    #     goods_table.delete_item(
    #         Key={
    #             "PK": item['PK'],
    #             "SK": item['SK']
    #         }
    #     )     

def build_item():

    helper = GetCodeFromProduct()
    helper.build()
    print('Total items', len(helper.SK2ITEM_DICT.keys()))
    count_item = 0
    stat_code = {}
    stat_SK = {}
    count = 0
    for id_cate, items in helper.NEW_CATEGORY_ITEM_SET.items():
        for item in items:  
            if item["CODE_PRODUCT"] == "NO_FOUND" or item["CODE_PRODUCT"] == "" or "CODE_PRODUCT" not in item.keys():
                count_item+=1 
            else:
                if "{}@{}".format(id_cate ,item["CODE_PRODUCT"]) not in stat_code.keys():
                    stat_code["{}@{}".format(id_cate, item["CODE_PRODUCT"])] = 1
                    stat_SK["{}@{}".format(id_cate, item["CODE_PRODUCT"])] = [item["SK"]]
                else:
                    stat_code["{}@{}".format(id_cate, item["CODE_PRODUCT"])] += 1 
                    stat_SK["{}@{}".format(id_cate, item["CODE_PRODUCT"])].append(item["SK"])
    print(count_item)

    for k, v in stat_code.items(): 
        if v != 1:
            count+=1
            for sk in stat_SK[k]:
                item = json.loads(json.dumps(helper.SK2ITEM_DICT[sk]), parse_float=Decimal) 
                put_goods_item_to_db(item) 
    
    print(count)

if __name__ == "__main__":
    build_item()