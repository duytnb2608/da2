import Vue from 'vue';
import Vuex from 'vuex';
import { CognitoUser } from '@aws-amplify/auth'

Vue.use(Vuex);

interface RootState {
  userName: string;
  user: CognitoUser;
}

const state: RootState = {
  userName: '',
  user: {} as CognitoUser,
}

export default new Vuex.Store({
  state,
  getters: {
    userName: state => state.userName,
    user: state => state.user
  },
  mutations: {
    setState(state, nextState) {
      Object.keys(nextState).forEach(key => ((state as any)[key as keyof RootState] = nextState[key]));
    }
  },
  actions: {
    signInDefault({ commit }, { user }) {
      commit('setState', { user })
    }
  },
  modules: {},
});
