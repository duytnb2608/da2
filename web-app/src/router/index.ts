import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import RouterHelper from './helper';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home.vue').catch(RouterHelper.handleAsyncComponentError),
    children: [
      {
        path: '',
        name: 'Home',
        component: () => import('@/components/pages/common/HomePage.vue').catch(RouterHelper.handleAsyncComponentError),
      },
      {
        path: 'category',
        name: 'Category',
        component: () =>
          import('@/components/pages/user/CategoryPage.vue').catch(RouterHelper.handleAsyncComponentError),
      },
      {
        path: 'product',
        name: 'Product',
        component: () =>
          import('@/components/pages/user/CategoryPage.vue').catch(RouterHelper.handleAsyncComponentError),
      },
    ],
  },
  {
    path: '/sys',
    name: 'sys',
    component: () => import('@/views/SysHome.vue').catch(RouterHelper.handleAsyncComponentError),
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '@/views/About.vue').catch(RouterHelper.handleAsyncComponentError),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
