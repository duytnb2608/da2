import AuthService from '@/api/auth.service';

export default class GoodsService {
  static async getGoodsItem(action = 'listAllItem'): Promise<any> {
    const url = process.env.VUE_APP_API_BASE_URL + `/dev/goods?action=${action}`;
    // const data = await AuthService.api.get(url).then((response) => response);
    return url;
  }
}
