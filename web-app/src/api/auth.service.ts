import axios from 'axios';
import Auth, { CognitoUser } from '@aws-amplify/auth';
import { AuthOptions } from '@aws-amplify/auth/lib-esm/types';
import AWS from 'aws-sdk';
import AWS4 from 'aws4';

export interface CognitoUserCustom extends CognitoUser {
  name?: string;
}
export interface ICredentials {
  AccessKeyId: string;
  Expiration: string;
  SecretAccessKey: string;
  SessionToken: string;
}

export interface ICredentialsService {
  executeApi?: ICredentials | null;
  // dynamodb?: ICredentials | null;
}

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
export default class AuthService {
  public static api = axios.create();
  private static config = {} as any;
  private static userCustom = {} as any;
  private static _currentCredentialsPromise = {
    executeApi: null as Promise<ICredentials | undefined | null> | null,
    // dynamodb: null as Promise<ICredentials | undefined | null> | null,
  };

  static async setCachedConfigure(): Promise<void> {
    AuthService.config = await Auth.configure({
      region: process.env.VUE_APP_AWS_REGION,
      authenticationFlowType: 'USER_SRP_AUTH',
      userPoolId: process.env.VUE_APP_SYSTEM_ADMIN_USER_POOL_ID,
      userPoolWebClientId: process.env.VUE_APP_SYSTEM_ADMIN_USER_POOL_WEB_CLIENT_ID,
      identityPoolId: process.env.VUE_APP_SYSTEM_ADMIN_IDENTITY_POOL_ID_1,
      storage: localStorage,
    } as AuthOptions);
  }

  static async signInDefault(): Promise<void> {
    AWS.config.region = 'ap-southeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: 'ap-southeast-1:10a4846d-343c-46f2-8bfb-151b81bba298',
    });

    await AuthService.setCachedConfigure();
    try {
      await Auth.signIn({
        username: process.env.VUE_APP_AWS_SYSTEM_ADMIN_USER_DEFAULT,
        password: process.env.VUE_APP_AWS_SYSTEM_ADMIN_PASSWORD_DEFAULT,
      });
    } catch (err) {
      console.log(err);
    }
  }

  static setCachedConfigAuth(userName: string): void {
    localStorage.setItem(`AuthCognitoConfig.${userName}`, JSON.stringify(AuthService.config));
  }

  static getCachedConfigAuth(userName: string): any {
    const config = localStorage.getItem(`AuthCognitoConfig.${userName}`);
    return config != null && Object.keys(config).length != 0 ? JSON.parse(config) : false;
  }

  static async currentCredentials(
    service: keyof ICredentialsService = 'executeApi',
    opts = { bypassCache: true }
  ): Promise<any> {
    if (!AuthService._currentCredentialsPromise[service]) {
      AuthService._currentCredentialsPromise[service] = AuthService.__currentCredentials(service, opts);
    }

    return (AuthService._currentCredentialsPromise[service] as Promise<ICredentials>)
      .then((r) => {
        AuthService._currentCredentialsPromise[service] = null;
        return r;
      })
      .catch((r) => {
        AuthService._currentCredentialsPromise[service] = null;
        throw r;
      });
  }

  static async __currentCredentials(
    service: keyof ICredentialsService = 'executeApi',
    opts = { bypassCache: true }
  ): Promise<any> {
    if (!AuthService.isAuthenticated()) {
      return null;
    }

    if (opts.bypassCache) {
      return (AuthService.getCachedCredentials() as any)[service];
    }

    const token = await AuthService.getJwtToken();
    const data = await AuthService.api
      .get(
        `${process.env.VUE_APP_API_BASE_URL}/dev/credentials?service=${
          service == 'executeApi' ? 'execute-api' : service
        }`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response.data.data;
      })
      .catch((err) => {
        console.log('err', err);
      });

    console.log('data', data);
    if (service == 'executeApi') {
      await AuthService.setCachedConfigure();
      AuthService._currentCredentialsPromise[service] = data.Credentials as Promise<ICredentials>;
    }
    return AuthService._currentCredentialsPromise[service];
  }

  static async isAuthenticated(): Promise<boolean> {
    try {
      if (Object.keys(AuthService.config).length > 0) return true;
      await AuthService.signInDefault();
      return false;
    } catch {
      await AuthService.signInDefault();
      return false;
    }
  }

  static async getUserDefault(): Promise<any> {
    try {
      return (await Auth.currentAuthenticatedUser()) as CognitoUser;
    } catch {
      console.log('err user not exist');
      return (await Auth.currentAuthenticatedUser()) as CognitoUser;
    }
  }

  static async getJwtToken(): Promise<string> {
    const user = await AuthService.getUserDefault();
    return (await user?.getSignInUserSession()?.getIdToken().getJwtToken()) || '';
  }

  static async getTimeLeftExpiration(): Promise<number> {
    const user = await AuthService.getUserDefault();
    return (await user?.getSignInUserSession()?.getIdToken().getExpiration()) || 0;
  }

  static setCachedCredentials(userName: string, credentialsService: ICredentialsService): any {
    if (credentialsService == null) localStorage.removeItem(`AuthCredentials.${userName}`);
    else localStorage.setItem(`AuthCredentials.${userName}`, JSON.stringify(credentialsService));
  }

  static async getCachedCredentials(): Promise<any> {
    const user = await this.getUserDefault();
    const credentialsRefresh: ICredentialsService = {
      // dynamodb: undefined,
    };

    let credentialsConfig: any = localStorage.getItem(`AuthCredentials.${user.getUsername()}`);
    credentialsConfig = credentialsConfig && credentialsConfig.length != 0 ? JSON.parse(credentialsConfig) : {};
    const configAuth: any = AuthService.getCachedConfigAuth(user.getUsername());
    return {
      ...credentialsRefresh,
      ...credentialsConfig,
      ...configAuth,
    };
  }
}

//https://github.com/axios/axios
AuthService.api.interceptors.request.use(async (config) => {
  const credentials = await AuthService.currentCredentials();
  if (!credentials) return config;
  const configExcuteApi = credentials['executeApi'];
  const opts: any = {
    host: new URL(process.env.VUE_APP_API_BASE_URL).host,
    service: 'execute-api',
    region: process.env.VUE_APP_AWS_REGION,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    method: config.method?.toUpperCase(),
  };

  AWS4.sign(opts, {
    accessKey: configExcuteApi.accessKey,
    secretAccessKey: configExcuteApi.secretAccessKey,
    sessionToken: configExcuteApi.sessionToken,
  });

  delete opts.headers['Host'];
  delete opts.headers['Content-Length'];

  opts.headers['Authorization'] = 'Bearer ' + (await AuthService.getJwtToken());
  config.headers = { ...opts.headers, ...config.headers };
  return config;
});
