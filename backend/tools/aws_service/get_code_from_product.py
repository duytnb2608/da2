import os
import sys 
import json 
import glob 
import tqdm
import base64

from tqdm import tqdm
from json import JSONEncoder 
from nlp2category import NLPCategory2Code     

file_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.abspath(file_dir+'/../..')
sys.path.append(os.path.normpath(root_dir)) 

class GetCodeFromProduct():
    
    def __init__(self):
        self.ID2CATE_DICT = {}
        self.CATE2ID_DICT = {}
        self.CATEGORY_ITEM_SET = {}
        self.NEW_CATEGORY_ITEM_SET = {}
        self.SK2ITEM_DICT = {}
        self.SENT2SK_DICT = {}
        self.MODEL = {}
        
    def load_data(self):
        self.load_id_cate_config()
        self.items = self.get_all_page_item_processed()

    def get_all_page_item_processed(self):

        domain_items = glob.glob("{}/*".format(os.path.join(file_dir,'data/goods'))) 
        list_items = []
        for domain_path in tqdm(domain_items): 
            categories = glob.glob("{}/*".format(os.path.join(domain_path)))
            for cate_path in categories:
                url_items = glob.glob("{}/*".format(os.path.join(cate_path))) 
                for url_path in url_items:
                    with open(os.path.join(url_path)) as f:
                        item = json.load(f)
                        f.close() 
                    list_items.append(item)
        return list_items

    def load_id_cate_config(self):
        with open(os.path.join(file_dir, 'category_config_custom.json'), 'r') as f:
            self.ID2CATE_DICT = json.load(f)  
 
        for k, v in self.ID2CATE_DICT.items():
            for cate in v:
                self.CATE2ID_DICT[cate] = k 

    def classify(self):
        for i in self.items:
            self.SK2ITEM_DICT[i['SK']] = i
            self.SENT2SK_DICT[i['NAME'].replace('-','')] = i["SK"]
        for item in self.items: 
            code = self.CATE2ID_DICT[item['CATEGORY_CUSTOM']]
            if code not in self.CATEGORY_ITEM_SET.keys():  
                self.CATEGORY_ITEM_SET[code] = [item]
            else:
                self.CATEGORY_ITEM_SET[code].append(item) 
                
    def get_all_item_by_attr(self, id_cate, attr="NAME"):
        items = self.CATEGORY_ITEM_SET[id_cate]
        if(attr == "URL"): return list(map(lambda x: self.urlsafre_decode(x["SK"].replace('-','')), items))
        return list(map(lambda x: x[attr].replace('-',''), items))
    
    def get_max_len_of_sentences(self, sents):
        map_item = list(map(lambda x: len(sents), sents))
        max_item = max(map_item)
        return sents[map_item.index(max_item)]
    
    def build(self):
        print("loading data...")
        self.load_data()
        self.classify()
        print("building...")
        for id_cate in tqdm(self.CATEGORY_ITEM_SET.keys()): 
            corpus = self.get_all_item_by_attr(id_cate, attr="NAME")
            corpus_url = self.get_all_item_by_attr(id_cate, attr="URL")
            model = NLPCategory2Code(id_cate, corpus, corpus_url) 
            model.build()
            self.MODEL[id_cate] = model
            new_category = []
            for index, sent in enumerate(corpus): 
                SK = self.SENT2SK_DICT[sent]  
                item = self.SK2ITEM_DICT[SK]
                item["LIST_CODE_PRODUCT"] = model.get_code_of_sentence(sent)
                item["CODE_PRODUCT"] = self.get_max_len_of_sentences(list(item["LIST_CODE_PRODUCT"].values()))
#                 init_code, filter_code, url_code = result['by_init'], result['by_name'], result['by_url']
#                 if init_code == filter_code: 
#                     if len(filter_code) != 0:
#                         item["CODE_PRODUCT"] = [init_code]
#                     else:
#                         item["CODE_PRODUCT"] = []
#                 else: 
#                     if len(init_code) > len(filter_code) and len(init_code) != 0:
#                         item["CODE_PRODUCT"] = [init_code]
#                     if len(init_code) < len(filter_code) and len(filter_code) != 0:
#                         item["CODE_PRODUCT"].append(filter_code)
#                     if len(init_code) == 0 or len(filter_code) == 0:
#                         item["CODE_PRODUCT"] = "NO_FOUND"
                new_category.append(item.copy())
            self.NEW_CATEGORY_ITEM_SET[id_cate] = new_category


    def urlsafre_encode(self, url):
        return base64.urlsafe_b64encode(url.encode("utf-8")).decode("utf-8").rstrip('=') if url else '' 

    def urlsafre_decode(self, encoded_str):
        return base64.urlsafe_b64decode(encoded_str + '===').decode("utf-8") if encoded_str else '' 
